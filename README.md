**BuGit**: File-less distributed issue tracking system with Git
===============================================================

[**BuGit**](https://gitlab.com/monnier/bugit) is an *issue tracking system*
(or *bug* tracking system) that stores its data in
a [Git](https://git-scm.com) repository in such a way that not only is it
possible to work offline, but the conflict resolution needed when sync'ing
concurrent modifications is 99% performed automatically by Git itself
rather than by BuGit (or by hand).
To that end, most of the info is not stored in files but in Git's metadata
and in file *names* instead.

Furthermore, it aims to enable sophisticated distributed operation, where
an issue may be shared between different databases.  E.g. a GNU/Linux
distribution may track an issue in its BuGit database and share that issue
with the upstream developer's own BuGit database, so that changes in one show
up in the other (while that issue may get different issue numbers in each
database).


Initial setup
-------------

The normal way to set up a central repository would go something like:

    bugit init -- --bare
    bugit post-receive --install

After which users can `git clone` the repository, work on it with the
commandline BuGit client and then use `bugit sync` to merge their changes
into the central repository.  The repository's post-receive hook will take
care of allocating numbers to issues and sending messages to the followers of
an issue.

Since BuGit uses branch names which all start with `bugit/` or `bugit-`, and
since it does not make use of the work area, instead of cloning that
repository into a fresh new repository, the user can also share the same
repository with the main project.  E.g.:

    cd ./myproject
    git remote add issues thehost:myproject-bugit-repository
    git config bugit.remote issues
    bugit sync

Usage
-----

You can get an overview of the commands supported by the command line client
by running it without any argument (or with just `bugit help`):

    BuGit usage:
    
    bugit init ...
    bugit new [--author=AUTHOR] [--usedesc] NAME [ATTACHMENT...]
    bugit description ISSUE
    bugit reply [--author=AUTHOR] [--editdesc] ISSUE [ATTACHMENT...]
    bugit show [--meta] ISSUE
    bugit list [ISSUE...]
    bugit allocate [ISSUE...]
    bugit sync [--push] [--pull] [--subset] [--postreceive] [REMOTE] [ISSUE...]
    bugit merge ISSUE DIR
    bugit id ISSUE
    bugit number ISSUE
    bugit name ISSUE [NEWNAME]
    bugit tag ISSUE [OPERATION] [TAG...]
    bugit attachment ISSUE [ATTACHMENT]
    bugit owner ISSUE [OPERATION] [EMAIL...]
    bugit follow ISSUE [OPERATION] [EMAIL...]
    bugit severity ISSUE [LEVEL]
    bugit search [--issues] PATTERN
    bugit help [COMMAND]
    bugit post-receive [--install] [NEXTCMD..]
    bugit cgi REPOSITORY
    bugit debbugs FROM TO

Furthermore, every subcommand comes with its own help:

### BuGit init

    bugit init ...

Initialize a new issue database.

Passes its arguments to `git init`.

### BuGit new

    bugit new [--author=AUTHOR] [--usedesc] NAME [ATTACHMENT...]

Create a new issue named NAME.

If AUTHOR is not specified it defaults to your current Git identity.
ATTACHMENTS... are extra files that are provided for this issue.
The command will open up an editor to let you type a description of
the problem.

With option `--usedesc` (or if the `bugit.usedescriptions` option is set),
the issue's description will be saved as an editable file rather than as an
immutable initial comment.

### BuGit description

    bugit description ISSUE

Return the description of issue ISSUE.

### BuGit reply

    bugit reply [--author=AUTHOR] [--editdesc] ISSUE [ATTACHMENT...]

Add information to issue ISSUE (can be an issue's NAME, ID or NUMBER).

Just like `bugit new`, it accepts attachments and will bring up an editor
to let you write a message.

With option `--editdesc`, this will let you edit the description of the issue
instead of adding an extra message.

### BuGit show

    bugit show [--meta] ISSUE

Display the messages in issue ISSUE.

If the `--meta` option is provided, the list will include messages about
changes to the meta-data, such as changes to the tags, name, severity, ...

### BuGit list

    bugit list [ISSUE...]

List existing issues.

If no ISSUE argument is provided, list all non-closed issues in the database.
Otherwise, only list the provided issues.

### BuGit allocate

    bugit allocate [ISSUE...]

Allocate issue numbers to the ISSUEs listed on the command line.

Upon creation, issues only have an ID and a NAME but not a NUMBER, since
allocating a number cannot be done reliably offline.  This command
is the one that will allocate a number for the provided issues.

In order for it to work well and not risk introducing conflicts, this should
normally be run on the central repository, typically from the post-receive
hook.

### BuGit sync

    bugit sync [--push] [--pull] [--subset] [--postreceive] [REMOTE] [ISSUE...]

Sync the local repository with the repository REMOTE.

REMOTE defaults to `origin` or the value of `bugit.remote`.

By default, this pulls and pushes all new changes, including new messages,
new issues, and new issue numbers.

With option `--pull`, this only pulls the remote changes.
With option `--push`, this only pushes the changes to the remote repository.

With option `--subset`, issue numbers are not propagated and only those
issues which already exist on REMOTE will be updated.

With option `--postreceive`, run the local post-receive hooks.
This is needed when running `bugit sync` from the master repository.

Finally if ISSUEs are provided, then only those issues's info are sync'd.
REMOTE is a name set up with `git remote` (which see).

### BuGit merge

    bugit merge ISSUE DIR

Merge the various branches existing for ISSUE.
DIR is the target directory into which the conflicted branch will be
checked out and where conflicts (if any) will need to be resolved manually.

### BuGit id

    bugit id ISSUE

Return the ID of the given ISSUE.

### BuGit number

    bugit number ISSUE

Return the NUMBER allocated to the given ISSUE.

### BuGit name

    bugit name ISSUE [NEWNAME]

If NEWNAME is provided, update the NAME of ISSUE to NEWNAME.
Otherwise, just return the NAME of ISSUE.

### BuGit tag

    bugit tag ISSUE [OPERATION] [TAG...]

Manipulate the set of TAGS applid to ISSUE.

If no OPERATION is specified, just return the set of tags applied to ISSUE.
OPERATION can be:

- `+`: add TAGs to ISSUE.
- `-`: remove TAGs from ISSUE.
- `=`: change the set of tags of ISSUE to be exactly TAGs.

### BuGit attachment

    bugit attachment ISSUE [ATTACHMENT]

Show attachments of ISSUE.
If ATTACHMENT is not provided, list ISSUE's attachments.

### BuGit owner

    bugit owner ISSUE [OPERATION] [EMAIL...]

Manipulate the set of persons assigned to ISSUE.

If no OPERATION is specified, just return the set of emails assigned to ISSUE.
OPERATION can be:

- `+`: add EMAILs to ISSUE's owners set.
- `-`: remove EMAILs from ISSUE's owners set.
- `=`: assign exactly EMAILs to ISSUE.

If no EMAILs are specified, it defaults to the current user.

### BuGit follow

    bugit follow ISSUE [OPERATION] [EMAIL...]

Manipulate the set of persons that wish to receive updates about the
status of ISSUE.

If no OPERATION is specified, just return the set of followers of ISSUE.
OPERATION can be:

-  `+`: add EMAILs to ISSUE's followers.
-  `-`: remove EMAILs from ISSUE's followers.
-  `=`: change the set of followers of ISSUE to be exactly EMAILs.

If no EMAILs are specified, it defaults to the current user.

### BuGit severity

    bugit severity ISSUE [LEVEL]

Manipulate the severity of ISSUE.

If no LEVEL is specified, just return the current severity of ISSUE.
Otherwise set its severity to LEVEL.

### BuGit search

    bugit search [--issues] PATTERN

Search messages that match PATTERN and output them.

PATTERN is interpreted as a regular expression.
If `--issues` is specified, then do not list the messages but list the issues
to which those messages belong instead.

### BuGit help

    bugit help [COMMAND]

Show a help summary

### BuGit post-receive

    bugit post-receive [--install] [NEXTCMD..]

If called with `--install`, this will install bugit as the Git *post-receive*
hook, so that changes pushed into the current repository get automatically
handled as follows:

- New messages are sent via email to the issue's followers.
- New issues get an issue number assigned and the author gets
  a confirmation email.

If called without option then this assumes we are being run as Git's
*post-receive* hook.

NEXTCMD is the command (with its args) to which to pass the post-receive info
that we don't use.  This is for sharing a single repository for bpth BuGit
and non-BuGit branches.  In that case NEXTCMD could be something like
`post-receive-email` so that updates to non-BuGit branches get their
diffs sent to some email address.

### BuGit cgi

    bugit cgi REPOSITORY

Run the Web UI using the CGI protocol.

REPOSITORY is the directory of the Git repository.

### BuGit debbugs

    bugit debbugs FROM TO

Convert DebBugs database to BuGit format.
FROM should be either the `db-h` or the `archive` subdirectory
of a DebBugs database.  TO should be any an empty target directory.


Git config vars used by BuGit
-----------------------------

Used by the command line BuGit client:

- `user.name`
- `user.email`
- `bugit.remote`: Default to use for REMOTE arguments.
- `bugit.usedescriptions`: If set to a non-empty string, new messages
  will be considered as the description of the issue.

Used by the central post-receive script:

- `bugit.mailhost`: host name to use for bugit's email addresses.
- `bugit.mailuser`: user name to use for bugit's email addresses.
- `bugit.sendmail`: command to use to send email.  Defaults to `sendmail`.
- `bugit.mailinglist`: email address to which we should send a copy of all new
  issue messages.
- `bugit.bugurl`: prefix to add to an issue ID to get a valid URL.

Design
------

The design is based on the idea of trying to represent the issue-database
in such a way that Git's merge takes care of our own merge needs.  IOW
Git's merge should only result in conflicts when there is a *real* conflict
that can only be resolved by hand (e.g. two concurrent changes to the title
of an issue).

The general idea is as follows:

- Keep messages in the metadata (more specifically the commit log), so
  they can't generate conflicts, they're auto-merged, and the ordering
  automatically preserved.
- Most other data is kept in file *names* (i.e. the files themselves are
  empty, to avoid merge conflicts).

### Issue identification

Numbering issues is an inherently centralized operation, so in BuGit's
distributed setting we need something more.

In BuGit, issues can be identified in 3 ways:

- **ID**: Issues have a unique and immutable identification called *id*, which
  is a random 32 digit lowercase-hexadecimal number.  This is the only stable
  and unambiguous identification.
- **NAME**: Issues have a *name*, also known as their *title*.  This is
  a human-readable text which is expected to describe the issue concisely (max
  200 bytes).  This property can change over time, and several issues can have
  the same *name*, tho this should indicate that those issues should probably
  be merged.
- **NB**: Issues can have a *number*.  This is not a property of the issue, tho,
  in the sense that the same issue can be known under different numbers in
  different databases.  So an issue can have several different NBs, and
  initially an issue has no NB at all (since allocation of an issue NB tends to
  be a centralized operation).  But in a given database, a given issue
  has at most one NB.

If BuGit says it wants a `ISSUE`, it means that you can give it any one of
those kinds of identifiers.

We could try to randomize the allocated number somewhat (basically add
some random number to $nb), so that issue-numbering can be done offline as
well!  But the amount of randomization needed depends on the rate of new
issues.  E.g. Debian's debbugs seems to be getting around 100 new issues per day,
so if we want to be able to "allocate today; push tomorrow" without having
a high risk of collision, the amount of randomization needed is fairly
high.  As randomization increases, issue numbers end up looking more like
issue IDs, so I'm not sure it's worth the trouble.

### Database layout

The database is layed out as follows:

- Every issue lives in its own branch named `bugit/<id>`.
  The branch's commit messages hold the issue's messages (in Markdown format,
  tho without embedded HTML tags).  The branch's files are as follows:

  - `name`: simple file holding the current *name* of this issue.
  - `description`: simple file which, if it exists, holds the description
    of the issue.
  - `attachments/<timestamp> - <name>` are attachments.
  - `followers/<email>` are empty files whose name indicates that `<email>`
    would like to receive updates on this issue.
  - `owners/<email>` are empty files whose name indicate that `<email>`
    is reponsible for this issue.
  - `tags/<tag>` are empty files indicating that `<tag>` is applied to
    the issue.
  - `severity/<n>` are empty files indicating that the severity of this
    issue should be considered as `<n>`.

Additionally to those issue branches, there are the following branches:
- `bugit-master`: contains files of the form `numbers/<nb>` containing 33B
  holding the *id* of issue number `<nb>` plus LF.
- `bugit-revmaps`: contains redundant reverse mappings.
  There are 2 kinds of such mappings:
  - `name/<name>/<id>` to map issue *names* to the corresponding *id*.
  - `<set>/<value>/<id>` to map particular set values
    to the issues that have them.

By design, the only possible sources of conflicts when merging different
databases are:

- if different issues have the same *number*.
- if a given issue *id* has different names.
- if an issue has two different attachments with the same name and same
  timestamp [ the timestamp should make this very unlikely, tho ].

